<?php

/**
 *
 *	@module			Forum
 *	@version		0.5.10
 *	@authors		Julian Schuh, Bernd Michna, "Herr Rilke", Dietrich Roland Pehlke (last)
 *	@license		GNU General Public License
 *	@platform		2.8.x
 *	@requirements	PHP 5.6.x and higher
 *
 */

require('../../config.php');
require(WB_PATH . '/modules/admin.php');

include_once(WB_PATH .'/framework/module.functions.php');

/**
 *        Load Language file
 */
$lang = (dirname(__FILE__))."/languages/". LANGUAGE .".php";
require_once ( !file_exists($lang) ? (dirname(__FILE__))."/languages/EN.php" : $lang );

require_once( dirname(__FILE__)."/classes/class.forum_parser.php" );
$parser = new forum_parser();

if (isset($_REQUEST['forumid'])) {
	$forum = $database->query("SELECT * FROM `" . TABLE_PREFIX . "mod_forum_forum` WHERE `forumid` = '" . intval($_REQUEST['forumid']) . "' AND `section_id` = '".$section_id."' AND `page_id` = '".$page_id."'");
	if ( 0 === $forum->numRows() ) {
		$admin->print_error(
			$MOD_FORUM['TXT_NO_ACCESS_F'],
			ADMIN_URL.'/pages/modify.php?page_id='.$page_id.'&section_id='.$section_id
		);
	}
	$forum = $forum->fetchRow();
	$fid = $_REQUEST['forumid'];
}

require_once(WB_PATH . '/modules/forum/backend.php');

if(!function_exists("forum_str2js")) {
	function forum_str2js(&$s) {
		$a = array(
			"\\'"	=> "&apos;",
			"\""	=> "&quot;",
			'&auml;' => "%E4",
			'&Auml;' => "%C4",
			'&ouml;' => "%F6",
			'&Ouml;' => "%D6",
			'&uuml;' => "%FC",
			'&Uuml;' => "%DC",
			'&szlig;' => "%DF",
			'&euro;' => "%u20AC",
			'$' => "%24"
		);
		$s = str_replace( array_keys($a), array_values($a), $s);
	}
}
echo '<div class="content-box legacy">';
ob_start();
print_forum_select_options( isset($forum) ? $forum['parentid'] : "" );
$forum_select_parent = ob_get_clean();

$page_values = array(
	'add_edit_titel'	=> (isset($forum['forumid']) ? $MOD_FORUM['TXT_EDIT_FORUM_B'].' - '.$forum['title'] : $MOD_FORUM['TXT_CREATE_FORUM_B']),
	'WB_URL'	=> WB_URL,
	'ADMIN_URL'	=> ADMIN_URL,
	'section_id'	=> $section_id,
	'page_id'		=> $page_id,
	'forum_id'		=> (isset($forum['forumid']) ? $forum['forumid'] : ''),
	'ftan'			=> (true === method_exists($admin, "getFTAN")) ? $admin->getFTAN() : "",
	'MOD_FORUM_TXT_SETTINGS_B'	=> $MOD_FORUM['TXT_SETTINGS_B'],
	'MOD_FORUM_TXT_TITLE_B'	=> $MOD_FORUM['TXT_TITLE_B'],
	'forum_title'	=> (isset($forum['title']) ? htmlspecialchars($forum['title']) : ''),
	'MOD_FORUM_TXT_DESCRIPTION_B'	=> $MOD_FORUM['TXT_DESCRIPTION_B'],
	'forum_description'	=> (isset($forum['description']) ? htmlspecialchars($forum['description']) : ''),
	'MOD_FORUM_TXT_DISPLAY_ORDER_B'	=> $MOD_FORUM['TXT_DISPLAY_ORDER_B'],
	'forum_displayorder'	=> (isset($forum['displayorder']) ? $forum['displayorder'] : ''),
	'MOD_FORUM_TXT_PARENT_FORUM_B'	=> $MOD_FORUM['TXT_PARENT_FORUM_B'],
	'forum_select_parent'	=> $forum_select_parent,
	'class_show_delete_forum'	=> ( isset($forum['forumid']) ? 'show_delete_forum' : 'hide_delete_forum' ),
	'MOD_FORUM_TXT_DELETE_B'	=> $MOD_FORUM['TXT_DELETE_B'],
	'MOD_FORUM_TXT_DELETE_FORUM_B'	=> $MOD_FORUM['TXT_DELETE_FORUM_B'],

	'MOD_FORUM_TXT_PERMISSIONS_B'	=> $MOD_FORUM['TXT_PERMISSIONS_B'],
	'MOD_FORUM_TXT_READ_B'	=> $MOD_FORUM['TXT_READ_B'],
	'MOD_FORUM_TXT_REGISTRATED_B'	=> $MOD_FORUM['TXT_REGISTRATED_B'],
	'MOD_FORUM_TXT_NOT_REGISTRATED_B'	=> $MOD_FORUM['TXT_NOT_REGISTRATED_B'],
	'MOD_FORUM_TXT_BOTH_B'		=> $MOD_FORUM['TXT_BOTH_B'],

	'forum_readaccess_reg_selected'	=> (isset($forum['readaccess']) && $forum['readaccess'] == 'reg' ? 'selected="selected"' : ''),
	'forum_readaccess_unreg_selected'	=> (isset($forum['readaccess']) && $forum['readaccess'] == 'unreg' ? ' selected="selected"' : ''),
	'forum_readaccess_both_selected'	=> (isset($forum['readaccess']) && $forum['readaccess'] == 'both' ? ' selected="selected"' : ''),

	'MOD_FORUM_TXT_WRITE_B'	=> $MOD_FORUM['TXT_WRITE_B'],
	'forum_writeaccess_reg_selected'	=> (isset($forum['writeaccess']) && $forum['writeaccess'] == 'reg' ? ' selected="selected"' : ''),
	'forum_writeaccess_unreg_selected'	=> (isset($forum['writeaccess']) && $forum['writeaccess'] == 'unreg' ? ' selected="selected"' : ''),
	'forum_writeaccess_both_selected'	=> (isset($forum['writeaccess']) && $forum['writeaccess'] == 'both' ? ' selected="selected"' : ''),

	'MOD_FORUM_TXT_SAVE_B'	=> $MOD_FORUM['TXT_SAVE_B'],
	'MOD_FORUM_TXT_RESET_B'	=> $MOD_FORUM['TXT_RESET_B'],
	'MOD_FORUM_TXT_CANCEL_B'	=> 'Zurück',
);

echo $parser->render(
	'add_edit_forum.lte',
	$page_values
);


	if(!isset($forum))
	{
	    //$admin->print_footer();
	    return 0;
	}

	$query = "SELECT * FROM `".TABLE_PREFIX."mod_forum_thread` WHERE `section_id`=".$section_id." AND `page_id`=".$page_id." AND `forumid`=".$forum['forumid']." ORDER BY `threadid` DESC";
	$result = $database->query( $query );
	if( true === $database->is_error() )
	{
	    die($database->get_error());
    }

	if(0 === $result->numRows())
	{
	    $admin->print_footer();
	    return 0;
	}
	$edit_link = WB_URL."/modules/forum/edit_post.php";

?>

<hr>

<h3>Posting Liste</h3>

<ul class="forum_list_postings">

	<? while($temp_post = $result->fetchRow(MYSQL_ASSOC)) : ?>
		<? $thread_title = $temp_post['title']; ?>

		<li class="thread">
			<div class="forum_list_action">
				<!-- edit -->
				<!-- close -->
				<!-- hide -->
				<!-- delete -->
				<a href="delete.php?tid=<?=$temp_post['threadid']?>&del=thread&fid=<?=$fid?>&page=<?=$page_id?>&section=<?=$section_id?>" class="delete-thread" title="löschen" data-title="<?=$thread_title?>">
					<i class="fa fa-lg fa-trash red"></i>
				</a>
			</div>
			<div class="forum_list_id"><b>[<?=$temp_post['threadid']?>]</b></div>
			<div class="forum_list_date"><b><?=date("Y-m-d - H:i",$temp_post['dateline'])?></b></div>
			<div class="forum_list_title"><b><?=$thread_title?></b></div>
		</li>

		<?
		$sub_query = "SELECT * FROM `".TABLE_PREFIX."mod_forum_post` WHERE `section_id`=".$section_id." AND `page_id`=".$page_id." AND `threadid`=".$temp_post['threadid']." ORDER BY `postid`";
		$sub_result = $database->query( $sub_query );
		if( true === $database->is_error() ) die($database->get_error());
		?>

		<? while($sub_post = $sub_result->fetchRow(MYSQL_ASSOC)) : ?>
			<? $post_title = substr(strip_tags($sub_post['text']),0,80).' ...' ?>
			<? $indent = '<i class="fa fa-share fa-flip-vertical">&nbsp;</i>'; ?>

			<li class="post">
				<div class="forum_list_action">
					<!-- edit -->
					<!-- close -->
					<!-- hide -->
					<!-- delete -->
					<a href="delete.php?tid=<?=$temp_post['threadid']?>&pid=<?=$sub_post['postid']?>&del=post&fid=<?=$fid?>&page=<?=$page_id?>&section=<?=$section_id?>" class="delete-post" title="löschen" data-title="<?=$post_title?>">
						<i class="fa fa-lg fa-trash red"></i>
					</a>
				</div>
				<div class="forum_list_id"><?=$indent?>[<?=$sub_post['postid']?>]</div>
				<div class="forum_list_date"><?=$indent?><?=date("Y-m-d - H:i",$sub_post['dateline'])?></div>
				<div class="forum_list_title">
					<a href="edit_post.php?tid=<?=$temp_post['threadid']?>&pid=<?=$sub_post['postid']?>&edit=post&fid=<?=$fid?>&page=<?=$page_id?>&section=<?=$section_id?>">
						<?=$indent?><?=$post_title?>
					</a>
				</div>
			</li>

		<? endwhile; ?>

	<? endwhile; ?>

</ul>
<div style="clear:both;"></div>
</div> <!-- content-box -->

<script>
	$(document).ready(function(){
		// confirm delete a whole thread
		$('.delete-thread').click(function() {
			var thread_title = $(this).attr('data-title')
			r = confirm('Wirklich den ganzen Thread\n' + thread_title + '\nlöschen?');
			return r;
		});
		// confirm delete a post
		$('.delete-post').click(function() {
			var thread_title = $(this).attr('data-title')
			r = confirm('Wirklich das Posting\n' + thread_title + '\nlöschen?');
			return r;
		});

	});
</script>
<?php
    $admin->print_footer();
?>
