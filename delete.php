<?
// init
require('../../config.php');
$db = new database();
$admin = new admin('forum', 'forum', true, false);

// get the get vars
$tid = $_GET['tid'];
$pid = $_GET['pid'];
$fid = $_GET['fid'];
$del = $_GET['del'];
$page_id = $_GET['page'];
$section_id = $_GET['section'];

// database handling delete post
if ($del == 'post') :
	// delete post
	$q = "DELETE FROM mod_forum_post WHERE postid = $pid";
	$db->query($q);

	// check replay counter
	$q = $db->query("SELECT * FROM mod_forum_thread WHERE threadid = $tid");
	$d = $q->fetchRow(MYSQL_ASSOC);
	$replys = $d['replycount'] - 1;

	if ($replys <= 0) :
		// no more replys so delete this thread
		$q = "DELETE FROM mod_forum_thread WHERE threadid = $tid";
		$db->query($q);
	else:
		// remainig replys so update the thread table
		// fetch the last postid
		$qp = $db->query("SELECT * FROM mod_forum_post WHERE threadid = $tid ORDER BY postid DESC LIMIT 1");
		$dp = $qp->fetchRow(MYSQL_ASSOC);

		if ($d['lastpostid'] != $dp['postid']) :
			$new_lastpostid = $dp['postid'];
		else :
			$new_lastpostid = $d['lastpostid'];
		endif;

		//update replycounter & lastpostid in thread table
		$q = "UPDATE mod_forum_thread SET lastpostid = $new_lastpostid, replycount = $replys WHERE threadid = $tid";
		$db->query($q);
	endif;
endif;

// database handling delete thread
if ($del == 'thread') :
	$q = "DELETE FROM mod_forum_post WHERE threadid = $tid";
	$db->query($q);

	$q = "DELETE FROM mod_forum_thread WHERE threadid = $tid";
	$db->query($q);
endif;

// remove Cache - we won't need it any more =) --- really ?!?!?
//$db->query("DELETE FROM " . TABLE_PREFIX . "mod_forum_cache WHERE section_id = '$section_id'");

// check for error and back
if($db->is_error()) die($db->get_error());

$backlink = WB_URL.'/modules/forum/addedit_forum.php?page_id='.$page_id.'&section_id='. $section_id.'&forumid='.$fid;

$admin->print_success("Erfolgreich gelöscht!", $backlink);
return 0;

