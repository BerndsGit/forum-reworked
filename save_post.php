<?
// init
require('../../config.php');
$db = new database();
$admin = new admin('forum', 'forum', true, false);
//print_r($_POST);

// get the post vars
$fid = $_POST['fid'];
$pid = $_POST['pid'];
$tid = $_POST['tid'];

$page_id = $_POST['page_id'];
$section_id = $_POST['section_id'];
$thread_title = $db->escapeString($_POST['thread_title']);
$text = $db->escapeString($_POST['text']);

// update db
$q = "UPDATE mod_forum_post SET text='$text' WHERE postid = $pid";
$db->query($q);

$q = "UPDATE mod_forum_thread SET title='$thread_title' WHERE threadid = $tid";
$db->query($q);

// check for error and back
if($db->is_error()) die($db->get_error());

$backlink = WB_URL.'/modules/forum/addedit_forum.php?page_id='.$page_id.'&section_id='. $section_id.'&forumid='.$fid;

$admin->print_success("Beitrag gespeichert!", $backlink);
return 0;
