<?

// init
require('../../config.php');
include WB_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php';
$db = new database();
$admin = new admin('forum', 'forum', true, false);

// get get vars
$tid = $_GET['tid'];
$pid = $_GET['pid'];
$edit = $_GET['post'];
$fid = $_GET['fid'];
$page_id = $_GET['page'];
$section_id = $_GET['section'];

// fetch post data
$q = $db->query("SELECT * FROM mod_forum_post WHERE postid = $pid");
$d = $q->fetchRow(MYSQL_ASSOC);
//print_r($d);
$post_text = $d['text'];

// fetch user data
if ($d['username'] != '') :
	$username = $d['username'];
else :
	$userid = $d['userid'];
	$qu = $db->query("SELECT * FROM users WHERE user_id = $userid");
	$du = $qu->fetchRow(MYSQL_ASSOC);
	$username = $du['display_name'].' (Admin)';
endif;

// fetch thread data
$qt = $db->query("SELECT * FROM mod_forum_thread WHERE threadid = $tid");
$dt = $qt->fetchRow(MYSQL_ASSOC);
$thread_title = $dt['title'];

define('TEMPLATE_DIR', 'musicando_forum');
?>

<div class="content-box legacy">
	<h2>Post #<?=$pid?> von <?=$username?> bearbeiten</h2>
	<form method="post" action="save_post.php">
		<input type="hidden" name="fid" value="<?=$fid?>">
		<input type="hidden" name="pid" value="<?=$pid?>">
		<input type="hidden" name="tid" value="<?=$tid?>">
		<input type="hidden" name="page_id" value="<?=$page_id?>">
		<input type="hidden" name="section_id" value="<?=$section_id?>">

		<input type="text" name="thread_title" value="<?=$thread_title?>" style="width:100%; margin-bottom:20px;">
		<div style="margin-bottom:20px;">
			<? show_wysiwyg_editor("text","text", $post_text,"100%","200px","ForumToolbar"); ?>
		</div>
		<button type="submit" name="save">Speichern</button>
		<button type="button" id="back-btn" style="float:right;">Zurück</button>
	</form>
</div>

<script>
	$(document).ready(function() {
		// back button
		$('#back-btn').click(function() {
			history.go(-1);
		});
	});

	/* ** init cke ****************************************************/
	// the toolbar
	CKEDITOR.config.toolbar_ForumToolbar = ([
		['Bold','Italic','Strike'],['FontSize','TextColor'],['RemoveFormat'],['NumberedList','BulletedList'],['Blockquote'],['SimpleLink'],['Image'],['Smiley'],['Source']
	]);
	CKEDITOR.toolbar = 'ForumToolbar';

	// mixed stuff
	//CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
	//CKEDITOR.config.shiftEnterMode = CKEDITOR.ENTER_P;
	var myEditor = CKEDITOR.instances.text;
	//myEditor.setActiveEnterMode(enterMode:2 , shiftEnterMode:1);
	//myEditor.setMode('source');

	// font color button
	CKEDITOR.config.colorButton_colors =
    '000,800000,8B4513,2F4F4F,008080,000080,4B0082,' +
    'B22222,DAA520,006400,40E0D0,0000CD,800080,' +
    'F00,FF8C00,FFD700,008000,0FF,00F,EE82EE,' +
    'FFA07A,FFA500,FFFF00,00FF00';
	CKEDITOR.config.colorButton_enableAutomatic = false;
	CKEDITOR.config.colorButton_enableMore = false;

	// smileys
	CKEDITOR.config.smiley_images = [
		'regular_smile.png','sad_smile.png','wink_smile.png','teeth_smile.png','confused_smile.png','tongue_smile.png',
		'embarrassed_smile.png','omg_smile.png','whatchutalkingabout_smile.png','angry_smile.png','angel_smile.png','shades_smile.png',
		'devil_smile.png','cry_smile.png','lightbulb.png','thumbs_down.png','thumbs_up.png'
	];
	CKEDITOR.config.smiley_descriptions = [
		'smiley', 'sad', 'wink', 'laugh', 'frown', 'cheeky', 'blush', 'surprise',
		'indecision', 'angry', 'angel', 'cool', 'devil', 'crying', 'enlightened', 'no',
		'yes'
	];
	CKEDITOR.config.smiley_columns = 6;

	// font sizes select
	CKEDITOR.config.fontSize_sizes = '10px;12px;14px;16px;24px';

	// simpler image dialog
    CKEDITOR.config.removeDialogTabs = 'image:advanced;image:Link;';
	CKEDITOR.on('dialogDefinition', function(ev) {
		// disable resizing of dialog windows
		ev.data.definition.resizable = CKEDITOR.DIALOG_RESIZE_NONE;

		// take the dialog name and its definition from the event data.
		var dialogName = ev.data.name;
		var dialogDefinition = ev.data.definition;

		// check if the definition is from the dialog we're interessted in
		if (dialogName == 'image') {
			// get a reference to the 'Image Info' tab
			var infoTab = dialogDefinition.getContents('info');

			// remove unnecessary widgets
			infoTab.remove('browse');
			infoTab.remove('txtHSpace');
			infoTab.remove('txtVSpace');
			infoTab.remove('txtBorder');
			infoTab.remove('cmbAlign');
			infoTab.remove('txtAlt');
		}
	});

	// css overwrites of editor
	CKEDITOR.addCss(".cke_editable {background-color: #ccc; color: #333;}");
	CKEDITOR.addCss(".cke_editable blockquote {border-left: 4px solid #888 !important; color: #666 !important;font-style: italic; font-weight: normal; margin-left: 20px;}");

</script>

<? $admin->print_footer(); ?>
