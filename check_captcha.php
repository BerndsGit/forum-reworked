<?
/***********************************************************************
 * precheck captcha on ajax request
 * module:	poetry_competition
 * author:	BerndJM
 * license:	WTFPL
 * version:	see info.php
 * init: 	2019-12-09 bjm
 **********************************************************************/

include "../../config.php";

if ($_POST['captcha'] != $_SESSION['captcha']) :
	echo 'false';
else :
	echo 'true';
endif;
